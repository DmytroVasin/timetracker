class Company < ApplicationRecord
  enum state: { disabled: 0, enabled: 1 }

  has_many :beacons
  has_many :areas
end

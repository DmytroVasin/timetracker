class BeaconsController < ApplicationController
  def index
    @beacons = Beacon.all
  end

  def show
    set_beacon
  end

  def new
    @beacon = Beacon.new
  end

  def edit
    set_beacon
  end

  def create
    @beacon = Beacon.new(beacon_params)

    if @beacon.save
      redirect_to @beacon, notice: 'Beacon was successfully created.'
    else
      render :new
    end
  end

  def update
    set_beacon

    if @beacon.update(beacon_params)
      redirect_to @beacon, notice: 'Beacon was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    set_beacon
    @beacon.destroy

    redirect_to beacons_url, notice: 'Beacon was successfully destroyed.'
  end

  private
  def set_beacon
    @beacon = Beacon.find(params[:id])
  end

  def beacon_params
    params.require(:beacon).permit(:state, :uniq_number, :description, :company_id, :area_id, :zone_id)
  end
end

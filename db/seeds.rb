require 'faker'

Beacon.destroy_all
Company.destroy_all
Area.destroy_all
Zone.destroy_all


company_1 = Company.create(state: 1, name: Faker::Company.name)
company_2 = Company.create(state: 1, name: Faker::Company.name)
company_3 = Company.create(state: 1, name: Faker::Company.name)


area_1 = Area.create(state: 1, company: company_1, name: Faker::Address.city)
area_2 = Area.create(state: 1, company: company_2, name: Faker::Address.city)
area_3 = Area.create(state: 1, company: company_3, name: Faker::Address.city)

zone_1 = Zone.create(state: 1, area: area_1, name: Faker::Address.street_name)
zone_2 = Zone.create(state: 1, area: area_2, name: Faker::Address.street_name)
zone_3 = Zone.create(state: 1, area: area_3, name: Faker::Address.street_name)

beacon_1 = Beacon.create(state: 1, uniq_number: Faker::Code.ean, description: Faker::Lorem.paragraph, company: company_1, area: area_1, zone: zone_1)
beacon_2 = Beacon.create(state: 1, uniq_number: Faker::Code.ean, description: Faker::Lorem.paragraph, company: company_2, area: area_2, zone: zone_2)
beacon_3 = Beacon.create(state: 1, uniq_number: Faker::Code.ean, description: Faker::Lorem.paragraph, company: company_3, area: area_3, zone: zone_3)

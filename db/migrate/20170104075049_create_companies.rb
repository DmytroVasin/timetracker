class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.integer :state
      t.string :name

      t.timestamps
    end
  end
end

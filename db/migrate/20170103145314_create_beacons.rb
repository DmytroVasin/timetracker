class CreateBeacons < ActiveRecord::Migration[5.0]
  def change
    create_table :beacons do |t|
      t.integer :state
      t.string :uniq_number
      t.text :description
      t.integer :company_id
      t.integer :area_id
      t.integer :zone_id

      t.timestamps
    end
  end
end

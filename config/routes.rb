Rails.application.routes.draw do
  resources :zones
  resources :areas
  root 'beacons#index'

  resources :beacons
  resources :companies
end

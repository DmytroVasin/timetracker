class CreateAreas < ActiveRecord::Migration[5.0]
  def change
    create_table :areas do |t|
      t.integer :state
      t.string :name
      t.integer :company_id

      t.timestamps
    end
  end
end

class Beacon < ApplicationRecord
  enum state: { disabled: 0, enabled: 1 }

  belongs_to :company
  belongs_to :area
  belongs_to :zone
end

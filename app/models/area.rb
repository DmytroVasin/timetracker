class Area < ApplicationRecord
  enum state: { disabled: 0, enabled: 1 }

  has_many :zones
  belongs_to :company
end

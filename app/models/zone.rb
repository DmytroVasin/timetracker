class Zone < ApplicationRecord
  enum state: { disabled: 0, enabled: 1 }

  belongs_to :area
end

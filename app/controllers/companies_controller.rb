class CompaniesController < ApplicationController

  def index
    @companies = Company.all
  end

  def show
    set_company
  end

  def new
    @company = Company.new
  end

  def edit
    set_company
  end

  def create
    @company = Company.new(company_params)

    if @company.save
      redirect_to @company, notice: 'Company was successfully created.'
    else
      render :new
    end
  end

  def update
    set_company
    if @company.update(company_params)
      redirect_to @company, notice: 'Company was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    set_company
    @company.destroy

    redirect_to companies_url, notice: 'Company was successfully destroyed.'
  end

  private

  def set_company
    @company = Company.find(params[:id])
  end

  def company_params
    params.require(:company).permit(:state, :name)
  end
end

class CreateZones < ActiveRecord::Migration[5.0]
  def change
    create_table :zones do |t|
      t.integer :state
      t.string :name
      t.integer :company_id
      t.integer :area_id

      t.timestamps
    end
  end
end
